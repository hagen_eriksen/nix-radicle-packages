# Inspired from https://github.com/NixOS/nixpkgs/blob/nixos-unstable/pkgs/applications/version-management/git-and-tools/radicle-upstream/default.nix#L78
{
  appimageTools,
  autoPatchelfHook,
  fetchurl,
  git-remote-rad,
  gsettings-desktop-schemas,
  gtk3,
  lib,
  pkgs,
  radicle-cli,
  radicleUpstreamAppImageSha,
  radicleUpstreamVersion,
  stdenv,
  undmg,
  zlib,
  ...
}:
let

  extractedSource = pkgs.appimageTools.extract {
    name = "radicle-upstream-${radicleUpstreamVersion}";
    src = source;
  };

  source = pkgs.fetchurl {
    url = "https://releases.radicle.xyz/radicle-upstream-${radicleUpstreamVersion}.AppImage";
    sha256 = radicleUpstreamAppImageSha;
  };
in appimageTools.wrapType2 rec {
  pname = "radicle-upstream";
  version = radicleUpstreamVersion;

  src = source;

  profile = ''
    export XDG_DATA_DIRS=${gsettings-desktop-schemas}/share/gsettings-schemas/${gsettings-desktop-schemas.name}:${gtk3}/share/gsettings-schemas/${gtk3.name}:$XDG_DATA_DIRS
  '';

  extraInstallCommands = ''
    mv $out/bin/${pname}-${version} $out/bin/${pname}
    ls -als
    
    # desktop item
    install -m 444 -D ${extractedSource}/${pname}.desktop $out/share/applications/${pname}.desktop
    substituteInPlace $out/share/applications/${pname}.desktop \
      --replace 'Exec=AppRun' 'Exec=${pname}'
    # icon
    install -m 444 -D ${extractedSource}/${pname}.png \
      $out/share/icons/hicolor/512x512/apps/${pname}.png
  '';

  extraPkgs = pkgs: [
    git-remote-rad
    radicle-cli
  ];

  meta = with lib; {
    description = "A decentralized app for code collaboration";
    homepage = "https://radicle.xyz/";
    license = licenses.gpl3Plus;
    maintainers = [ "Yves Zumbach<yves@zumbach.dev>" ];
    platforms = [ "x86_64-linux" ];
  };
}
