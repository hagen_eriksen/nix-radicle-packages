# Nix Radicle Packages

Nix packages for various Radicle clients.

## Version Updates

If any of the packages in this repo are not up-to-date, please open an issue indicating with package is not up-to-date, so that I can fix it.
