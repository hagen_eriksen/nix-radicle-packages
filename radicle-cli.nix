# Inspired from https://github.com/viraptor/radicle-nix/blob/master/flake.nix
# See https://nixos.wiki/wiki/Rust for instructions on using fenix instead of oxalica/rust-overlay

{ git-remote-rad, lib, pkgs, rustPlatform, ... }:

# Versions, URL, cargoHash
#
# - 0.3.1: "git+https://seed.alt-clients.radicle.xyz/radicle-cli.git?ref=master&rev=2048dfd8424860c704f408cc17cf42798d0d9afd", "sha256-UJdoXr02XPAsGopKbH7ZqK8ktMBMK6IfcUBH8j3Tt4g="

rustPlatform.buildRustPackage rec {

  pname = "radicle-cli";
  version = "0.6.0";

  src = pkgs.fetchFromGitHub {
    owner = "radicle-dev";
    repo = pname;
    rev = "v${version}"; # rev = "v${version}"; (0.5.1's lock file is broken)
    sha256 = "sha256-43dckx37qNBytKtyBS4uUpQ4cIFlQ7+i8NXewgmBABw=";
  };

  nativeBuildInputs = [
    pkgs.asciidoctor
    pkgs.cmake
    pkgs.openssl.dev
    pkgs.pkg-config
    # pkgs.rust-bin.stable.latest.minimal
  ];

  buildInputs = [
    git-remote-rad
    pkgs.openssl
  ];
  
  cargoSha256 = "sha256-NRHMdOmuUelUTFD6tFQsaJ30px0xNgR3WlBkuNOwFLY=";

  postInstall = ''
    bash ./.github/build-man-page.bash rad.1.adoc
    mkdir -p $out/share/man/man1
    cp rad.1.gz $out/share/man/man1/rad.1.gz
    rm -f $out/bin/git-remote-rad
  '';

  doCheck = false; # https://github.com/radicle-dev/radicle-cli/issues/84
  
  # PKG_CONFIG_PATH = "${pkgs.openssl.dev}/lib/pkgconfig";

  meta = {
    lib.description = "A command-line interface for Radicle.";
    lib.homepage = "https://radicle.xyz";
    lib.license = lib.licenses.gpl3;
    lib.maintainers = [ "Yves Zumbach<yves@zumbach.dev>" ];
  };
}
