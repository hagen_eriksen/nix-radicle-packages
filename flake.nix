{
  description = "Nix packages for various Radicle clients.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    devshell = {
      url = "github:numtide/devshell";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, devshell, flake-utils, nixpkgs, rust-overlay }:
  flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [
          devshell.overlay
          rust-overlay.overlays.default
        ];
      };
    in {
      # devShell = # Used by `nix develop`, value needs to be a derivation
      #   pkgs.devshell.mkShell {
      #     # TODO Specify which packages to add to the shell environment
      #     packages = [
      #       packages.radicle-cli
      #     ];
      #   };

      # nix build [--debugger] ".#<name>" -- <args>
      packages = let
        radicleUpstreamAppImageSha = "sha256-u8X1W9va3S9vvsbDlj6iGhPBRjSyvshJm9l+8nuYnEI=";
        radicleUpstreamVersion = "0.3.1";
      in {

        default = self.packages."${system}".radicle-cli;

        git-remote-rad = pkgs.callPackage ./git-remote-rad.nix {
          inherit radicleUpstreamAppImageSha radicleUpstreamVersion;
        };

        radicle-cli = pkgs.callPackage ./radicle-cli.nix {
          git-remote-rad = self.packages."${system}".git-remote-rad;
        };

        radicle-upstream = pkgs.callPackage ./radicle-upstream.nix {
          inherit radicleUpstreamAppImageSha radicleUpstreamVersion;
          git-remote-rad = self.packages."${system}".git-remote-rad;
          radicle-cli = self.packages."${system}".radicle-cli;
        };
      };

      # nix run ".#<name>" -- <args>
      apps = {

        default = self.apps."${system}".radicle-cli;

        radicle-cli = flake-utils.lib.mkApp {
          drv = self.packages."${system}".radicle-cli;
          exePath = "/bin/rad";
        };

        radicle-upstream = flake-utils.lib.mkApp {
          drv = self.packages."${system}".radicle-upstream;
          exePath = "/bin/radicle-upstream";
        };
      };
    }
  );
}