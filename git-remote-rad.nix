{
  autoPatchelfHook,
  lib,
  pkgs,
  radicleUpstreamAppImageSha,
  radicleUpstreamVersion,
  stdenv,
  zlib,
  ...
}:
stdenv.mkDerivation {
  pname = "git-remote-rad";
  version = radicleUpstreamVersion;

  src = pkgs.appimageTools.extract {
    name = "radicle-upstream-${radicleUpstreamVersion}";
    src = pkgs.fetchurl {
      url = "https://releases.radicle.xyz/radicle-upstream-${radicleUpstreamVersion}.AppImage";
      sha256 = radicleUpstreamAppImageSha;
    };
  };

  nativeBuildInputs = [ autoPatchelfHook ];
  buildInputs = [ zlib ];

  installPhase = ''
    mkdir -p $out/bin/
    cp resources/git-remote-rad $out/bin/git-remote-rad
  '';
}
